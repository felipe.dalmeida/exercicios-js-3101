var readlineSync = require("readline-sync");

// Wait for user's response.
var userName = readlineSync.question("May I have your name? ");
console.log("Hi " + userName + "!");

// Handle the secret text (e.g. password).
var favFood = readlineSync.question("What is your favorite food? ", {
	hideEchoBack: true, // The typed text on screen is hidden by `*` (default).
});
console.log("Oh, " + userName + " loves " + favFood + "!");

/**
 * ------------------- COMO EXECUTAR --------------------
 * - Abra o terminial na pasta raiz;
 * - Instale as dependências com o comando yarn install ou npm install (apenas na primeira vez);
 * - Após finalizar a instalação, execute o arquivo com: node <nome_do_arquivo>
 */