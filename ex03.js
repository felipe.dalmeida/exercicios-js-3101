/**
 * Faça um programa que receba 3 número inteiros ou decimais, valide e retorne uma mensagem para cada situação.
 * 
 * - se a média for <= 1, retorne “Aluno reprovado”;
 * - se a média for >= 2 e <= 4, retorne “Aluno em recuperação”;
 * - se a média for >= 5 e <= 7, retorne “Aluno aprovado”;
 * - se a média for > 7, retorne “Aluno aprovado com ótimo aproveitamento”;
 *
 * OBS: para calcular a média some todos os números e divide pelo total de números.
 */
